//First install Docker for Windows
/**
 * Then you need to install kubectl - Command line utility and add to Env Variable Path
 * Then you can install Minicube for Single Node cluster Steup ans Add minicube exe to Env Var Path
 * It will prepare your Cluster in Local Machine - 🏄  Done! kubectl is now configured to use "minikube" cluster and "default" namespace by default
 * minikube status -> Status of cluster
 * 
 * minikube
type: Control Plane
host: Running
kubelet: Running
apiserver: Running
kubeconfig: Configured

# You can set What Driver Minikube will be using 
minikube start --driver=docker
# You can Set default Driver for your System 
minikube config set driver docker
# Follow Below Link to Use Hyper V as Driver in Windows 

https://minikube.sigs.k8s.io/docs/drivers/hyperv/


Now we need to verify if Kubectl Utility is Working or Not 
kubelet get nodes - this command you can run 

NAME       STATUS   ROLES                  AGE     VERSION
minikube   Ready    control-plane,master   4m17s   v1.23.3
 
The above output shows the Nodes in the Cluster - Here Single Node actic as Master as well as Node [Worker]

Deply application to Kubernetes 

kubectl create deployment hello-minikube --image=k8s.gcr.io/echoserver:1.4
kubectl expose deployment hello-minikube --type=NodePort --port=8080

kubectl get deployments  hello-minikube

NAME             READY   UP-TO-DATE   AVAILABLE   AGE
hello-minikube   1/1     1            1           22m

Check Service Hosted on Kubernetes 
kubectl get services hello-minikube

NAME             TYPE       CLUSTER-IP     EXTERNAL-IP   PORT(S)          AGE
hello-minikube   NodePort   10.110.2.252   <none>        8080:32400/TCP   50s

kubectl port-forward service/hello-minikube 7080:8080 - Use this if you are using Docker Desktop

### --Clenup Process

 kubectl delete service hello-minikube 
kubectl delete  deployment hello-minikube
kubectl get pods - In some time it will be empty

 minikube stop   - Stopping the kubernetes Cluster

  minikube delete --all - Delete all Minikube clusters 

  


  #Creation of Pod 
  kubectl run nginx --image=nginx

   kubectl describe pod nginx

 kubectl get pods -o wide


minikube tunnel - to Open a Network Tunnel 

# fOR TAINTING nODES 
kubectl taint nodes node-1 app=blue:NoSchedule
 */

